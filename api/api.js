const request = require('request');
const auth = require('./auth.js');

// DarkSky API key
const apiKey = '8d15bebf8d1003e410ff3540494c19b3';

module.exports = new function () {
   this.get = get;
};

// Get weather based on latitude and longitude for the last full week (Monday to Sunday)
function get(parameters) {
   let { latitude, longitude, token } = parameters;

   // Parameter validation and normalization
   latitude = Math.min(Math.max(parseFloat(latitude), -90), 90);
   longitude = Math.min(Math.max(parseFloat(longitude), -180), 180);

   if (isNaN(latitude) || isNaN(longitude)) {
      return Promise.reject({ message: "Latitude or longitude not a number." });      
   }

   // Check if token is valid before proceeding with weather data requests
   if (auth.verifyToken(token)) {
      const timestamps = getTimestamps();
      const promises = timestamps.map(timestamp => {
         const url = `https://api.darksky.net/forecast/${apiKey}/${latitude},${longitude},${timestamp}?exclude=currently,minutely,hourly,alerts,flags`;

         const promise = new Promise((resolve, reject) => {
            request(url, (error, response, body) => {
               const data = body && JSON.parse(body);
               error = data && data.error || error;
               if (error) {
                  reject(error);
               }
               else if (data.daily) {
                  resolve({ latitude, longitude, timestamp, response: data });
               }
               else {
                  reject({ message: "No daily weather data." });               
               }
            });
         });
         return promise;
      });
      return Promise.all(promises).then(data => getWeather(data));
   }
   else {
      return Promise.reject({ message: "Authentication error." });
   }
}

// Build weather object with dates as keys
function getWeather(data) {
   const weather = data.reduce((weather, day) => {
      weather[getDate(day.timestamp)] = day;
      return weather;
   }, {});
   return weather;
}

// Get YYYY-MM-DD date representation for timestamp
function getDate(timestamp) {
   const date = new Date(timestamp * 1000);

   const year = date.getFullYear();
   const month = ('0' + (date.getMonth() + 1)).slice(-2);
   const day = ('0' + date.getDate()).slice(-2);

   return `${year}-${month}-${day}`;
}

// Get timestamps for all the days of the previous week (Monday to Sunday)
function getTimestamps() {
   let date = new Date();
   
   const year = date.getFullYear();
   const month = date.getMonth();
   const day = date.getDate();
   const weekDay = date.getDay() || 7;
   const offset = - 6 - weekDay;

   let timestamps = [];
   for (var i = 0; i < 7; i++) {
      date = new Date(year, month, day + offset + i);
      timestamps.push(date.getTime() / 1000);
   }
   return timestamps;
}