const jws = require('jws');

const secret = 'ArcusW3@th3rS3cr3t!';
const algorithm = 'HS256';

module.exports = new function Auth() {
   this.createToken = createToken;
   this.verifyToken = verifyToken;
   this.decodeToken = decodeToken;

   // Log a valid token
   console.log(createToken({ id: 0, name: 'Matt Yunker', email: 'myu@arcusnext.com' }));
};

// Create JSON web token for id, name and email
function createToken(user = { id, name, email }) {
   return jws.sign({
      header: {
         typ: 'JWT',
         alg: algorithm
      },
      payload: {
         iss: 'ArcusWeather',
         data: {
            id: user.id,
            name: user.name,
            email: user.email,
            created: Math.round(new Date().getTime() / 1000),
         }
      },
      secret: secret,
   });
}

// Verify data of JSON web token 
function verifyToken(token) {  
   return !!jws.decode(token) && jws.verify(token, algorithm, secret);
}

// Decode JSON web token to get data like id, name and email
function decodeToken(token) {
   const data = jws.decode(token);
   return data && data.payload.data;
}