
const express = require('express');
const bodyParser = require('body-parser');
const api = require('./api/api.js');

const app = express();

// Serve static files from public folder
app.use(express.static('public'))

// Enable post request body parsing for both form submitted and JSON parameters
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json());

// Get weather data from the API get method and send the result as JSON
app.post('/api/get/', (request, response) => {   
   api.get(request.body)
      .then(weather => response.json({ weather }))
      .catch(error => response.json({ error }));
});

// Start the weather server
app.listen(3000, () => {
   console.log('Arcus Weather is listening on port 3000');
});
