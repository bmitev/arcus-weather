PROJECT_ID=$(google-cloud-sdk/bin/gcloud config get-value project -q)
IMAGE=gcr.io/$PROJECT_ID/arcus-weather:$(git log --format="%h" -n 1)
HOST_PORT=8080
CONTAINER=arcus-weather-container
CONTAINER_PORT=3000
CLUSTER=arcus-weather-cluster
DEPLOYMENT=acrus-weather-deployment

# Docker build

# Update source from git repository
git pull
# Update node docker image
docker pull node:carbon
# Build docker image from source
docker build -t $IMAGE .

# Development deploy

# Stop running local docker container instance
docker stop $CONTAINER > /dev/null
# Remove local docker container instance
docker rm $CONTAINER > /dev/null
# Start local docker container from image
docker run --name=$CONTAINER --restart=always -p $HOST_PORT:$CONTAINER_PORT -d $IMAGE
# Cleanup unused local images
docker image prune --force

# Production init

# Push docker image to GCP registry
# google-cloud-sdk/bin/gcloud docker -- push $IMAGE
# Retreive GCP cluster credentials and configure kubectl
# google-cloud-sdk/bin/gcloud container clusters get-credentials $CLUSTER
# Create GCP deployment from docker image
# google-cloud-sdk/bin/kubectl run $DEPLOYMENT --image=$IMAGE --port $CONTAINER_PORT
# Expose GCP deployment ports to the internet
# google-cloud-sdk/bin/kubectl expose deployment $DEPLOYMENT --type=LoadBalancer --port $HOST_PORT --target-port $CONTAINER_PORT

# Production update

# Push docker image to GCP registry
google-cloud-sdk/bin/gcloud docker -- push $IMAGE
# Apply update to GCP deployment
google-cloud-sdk/bin/kubectl set image deployment/$DEPLOYMENT $DEPLOYMENT=$IMAGE
